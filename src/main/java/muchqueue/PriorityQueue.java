package muchqueue;

import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Generic threadsafe PriorityQueue class.
 * Implementation is based on having the priority separate from the containing elements and used as a key.
 * @param <K> priority - used as key, must be comparable
 * @param <E> element to be stored, must be comparable
 */
public class PriorityQueue <K extends Comparable<K>, E extends Comparable<E>> {

    private TreeMap<K, List<E>> priorityToWaitList = new TreeMap<>(Collections.reverseOrder());

    private Map<K, ReadWriteLock> waitListLocks = new HashMap<>();

    private Lock lockForLocks = new ReentrantLock();

    private ReadWriteLock masterLock = new ReentrantReadWriteLock();

    /**
     * Enqueue method used for adding elements to the queue
     * O(log n)
     * @param priority
     * @param element
     */
    public void enqueue(K priority, E element) {
        Lock writeLock = lockWaitList(priority, false, true);
        try {
            List<E> waitList = priorityToWaitList.get(priority);
            if(waitList == null) {
                waitList = new LinkedList<>();
                masterLock.writeLock().lock();
                try {
                    priorityToWaitList.put(priority, waitList);
                } finally {
                    masterLock.writeLock().unlock();
                }
            }
            waitList.add(element);
        } finally {
            writeLock.unlock();
        }
    }

    /**
     * Remove method for a specific priority and element
     * O(log n)
     * @param priority
     * @param element
     */
    public void remove(K priority, E element) {
        Lock waitLock = lockWaitList(priority, false, false);
        try {
            List<E> waitList = priorityToWaitList.get(priority);
            if (waitList == null || waitList.isEmpty()) {
                throw new IllegalArgumentException("Invalid priority");
            }
            E storedElement = waitList.stream()
                    .filter(e -> Objects.equals(e, element))
                    .findAny()
                    .orElse(null);
            if (storedElement == null) {
                throw new IllegalArgumentException("Invalid element");
            }
            waitList.remove(storedElement);
            if(waitList.isEmpty()) {
                handleEmptyWaitList(priority, waitLock);
            }
        } finally {
            waitLock.unlock();
        }
    }

    /**
     * Method for removing the element from the top of the queue. Elements with the highest priority are chosen.
     * If there is more than one element with the highest priority the oldest one is chosen.
     * O(log n)
     * @return element
     */
    public E dequeue() {
        Lock writeLock;
        K priority;

        masterLock.readLock().lock();
        try {
            priority = priorityToWaitList.firstKey();
            writeLock = lockWaitList(priority, false, false);
        } finally {
            masterLock.readLock().unlock();
        }

        try {
            List<E> waitList = priorityToWaitList.get(priority);
            if (waitList == null) {
                throw new NoSuchElementException("Queue is empty");
            }
            E element = waitList.get(0);
            waitList.remove(0);
            if(waitList.isEmpty()) {
                handleEmptyWaitList(priority, writeLock);
            }
            return element;
        } finally {
            writeLock.unlock();
        }
    }

    /**
     * Method for returning the element from the top of the queue. Elements with the highest priority are chosen.
     * If there is more than one element with the highest priority the oldest one is chosen.
     * O(log n)
     * @return element
     */
    public E peek() {
        Lock readLock;
        K priority;

        masterLock.readLock().lock();
        try {
            priority = priorityToWaitList.firstKey();
            readLock = lockWaitList(priority, true, false);
        } finally {
            masterLock.readLock().unlock();
        }

        try {
            List<E> waitList = priorityToWaitList.get(priority);
            if (waitList == null) {
                throw new NoSuchElementException("Queue is empty");
            }
            E element = waitList.get(0);
            return element;
        } finally {
            readLock.unlock();
        }
    }

    /**
     * Method for returning all elements in the queue. Elements are sorted according to priority
     * O(n^2)
     * @return list of all elements in the queue
     */
    public List<E> toList() {
        List<E> result = new ArrayList<>();
        masterLock.readLock().lock();
        try {
            priorityToWaitList.keySet().forEach(p -> {
                List<E> waitList = priorityToWaitList.get(p);
                result.addAll(waitList);
            });
        } finally {
            masterLock.readLock().unlock();
        }
        return result;
    }

    private Lock lockWaitList(K priority, boolean isReadLock, boolean createIfNecessary) {
        Lock waitLock;
        lockForLocks.lock();
        try {
            ReadWriteLock readWriteLock = waitListLocks.get(priority);
            if (readWriteLock == null) {
                if (createIfNecessary) {
                    readWriteLock = new ReentrantReadWriteLock();
                    waitListLocks.put(priority, readWriteLock);
                } else {
                    throw new IllegalArgumentException("Invalid priority");
                }
            }
            waitLock = isReadLock ? readWriteLock.readLock() : readWriteLock.writeLock();
            waitLock.lock();
        } finally {
            lockForLocks.unlock();
        }
        return waitLock;
    }

    private void removeWaitListLock(Lock lock) {
        lockForLocks.lock();
        try {
            waitListLocks.remove(lock);
        } finally {
            lockForLocks.unlock();
        }
    }

    private void handleEmptyWaitList(K priority, Lock waitLock) {
        masterLock.writeLock().lock();
        try {
            priorityToWaitList.remove(priority);
        } finally {
            masterLock.writeLock().unlock();
        }
        removeWaitListLock(waitLock);
    }
}