package muchqueue;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


/**
 * Test class for PriorityQueue
 */
public class PriorityQueueTest {

    private PriorityQueue<Long, Long> underTest;

    @Before
    public void init() {
        underTest = new PriorityQueue<>();
    }

    @Test
    public void test_simple_enqueue_dequeue() {
        underTest.enqueue(1L, 2L);

        long result = underTest.dequeue();

        assertEquals(2L, result);
    }

    @Test
    public void test_enqueue_dequeue_order() {
        underTest.enqueue(2L, 4L);
        underTest.enqueue(2L, 3L);
        underTest.enqueue(1L, 2L);

        long result = underTest.dequeue();
        assertEquals(4L, result);

        result = underTest.dequeue();
        assertEquals(3L, result);

        result = underTest.dequeue();
        assertEquals(2L, result);
    }

    @Test(expected = NoSuchElementException.class)
    public void test_dequeue_empty() {
        underTest.enqueue(1L, 2L);
        long result = underTest.dequeue();
        assertEquals(2L, result);
        underTest.dequeue();
    }

    @Test
    public void test_peek() {
        underTest.enqueue(2L, 4L);
        underTest.enqueue(2L, 3L);
        underTest.enqueue(1L, 2L);

        long result = underTest.peek();
        assertEquals(4L, result);
        result = underTest.dequeue();
        assertEquals(4L, result);

        result = underTest.peek();
        assertEquals(3L, result);
        result = underTest.dequeue();
        assertEquals(3L, result);

        result = underTest.peek();
        assertEquals(2L, result);
        result = underTest.dequeue();
        assertEquals(2L, result);
    }

    @Test(expected = NoSuchElementException.class)
    public void test_peek_empty() {
        underTest.peek();
    }


    @Test
    public void test_toList_order() {
        //adding elements in random order, also with duplicates
        underTest.enqueue(2L, 2L);
        underTest.enqueue(2L, 2L);
        underTest.enqueue(6L, 6L);
        underTest.enqueue(3L, 3L);
        underTest.enqueue(1L, 1L);
        underTest.enqueue(9L, 9L);
        underTest.enqueue(9L, 9L);
        underTest.enqueue(8L, 8L);
        underTest.enqueue(7L, 7L);

        List<Long> result = underTest.toList();

        assertEquals(Arrays.asList(9L, 9L, 8L, 7L, 6L, 3L, 2L, 2L, 1L), result);
    }

    @Test
    public void test_toList_empty() {
        List<Long> result = underTest.toList();
        assertTrue(result.isEmpty());
    }

    @Test
    public void test_remove() {
        underTest.enqueue(3L, 5L);
        underTest.enqueue(3L, 3L);
        underTest.enqueue(2L, 4L);
        underTest.enqueue(2L, 3L);
        underTest.enqueue(1L, 2L);

        underTest.remove(2L, 3L);
        List<Long> result = underTest.toList();

        assertEquals(Arrays.asList(5L, 3L, 4L, 2L), result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void test_remove_invalid_priority() {
        underTest.enqueue(2L, 4L);
        underTest.enqueue(2L, 3L);
        underTest.enqueue(1L, 2L);

        underTest.remove(3L, 1L);
    }

    @Test(expected = IllegalArgumentException.class)
    public void test_remove_invalid_element() {
        underTest.enqueue(2L, 4L);
        underTest.enqueue(2L, 3L);
        underTest.enqueue(1L, 2L);

        underTest.remove(2L, 5L);
    }

    @Test(expected = IllegalArgumentException.class)
    public void test_remove_empty() {
        underTest.remove(1L, 1L);
    }
}