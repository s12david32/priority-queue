package muchqueue;

import org.junit.Before;
import org.junit.Test;

import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.Assert.assertEquals;


/**
 * Test class for PriorityQueue with parallel execution
 */
public class PriorityQueueParallelTest {

    private static final int KEY_LIMIT = 50;
    private static final int VALUE_LIMIT = 100;

    private PriorityQueue<Long, Long> underTest;

    @Before
    public void init() {
        underTest = new PriorityQueue<>();
    }

    @Test
    public void testTenThreads_InOrder() throws ExecutionException, InterruptedException {
        test(10, false);
        assertEquals(Arrays.asList(9L, 8L, 7L, 6L, 5L, 4L, 3L, 2L, 1L, 0L), underTest.toList());
    }

    @Test
    public void testFiftyThreads_Random() throws ExecutionException, InterruptedException {
        test(50, true);
        assertEquals(50, underTest.toList().size());
    }

    public void test(int threads, boolean randomize) throws ExecutionException, InterruptedException {
        ExecutorService service =
                Executors.newFixedThreadPool(threads);
        CountDownLatch latch = new CountDownLatch(1);
        AtomicBoolean running = new AtomicBoolean();
        AtomicInteger overlaps = new AtomicInteger();
        Collection<Future<Integer>> futures =
                new ArrayList<>(threads);
        for (int t = 0; t < threads; ++t) {
            Long key;
            Long value;
            if (randomize) {
                key = (long) Math.random() * KEY_LIMIT + 1;
                value = (long) Math.random() * VALUE_LIMIT + 1;
            } else {
                key = Long.valueOf(t);
                value = Long.valueOf(t);
            }
            futures.add(
                    service.submit(
                            () -> {
                                try {
                                    latch.await();
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                                if (running.get()) {
                                    overlaps.incrementAndGet();
                                }
                                running.set(true);
                                underTest.enqueue(key, value);
                                running.set(false);
                                return key.intValue();
                            }
                    )
            );
        }
        latch.countDown();
        Set<Integer> ids = new HashSet<>();
        for (Future<Integer> f : futures) {
            ids.add(f.get());
        }
    }
}